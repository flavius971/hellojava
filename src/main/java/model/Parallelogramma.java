package model;

public class Parallelogramma {
	private double latoMaggiore;
	private double latoMinore;
	private double altezza;
	private double perimetro;
	private double area;
	private double dMaggiore;
	private double dMinore;
	
	public double Perimetro() {
		perimetro=2*latoMaggiore+2*latoMinore;
		return perimetro;	
	}
	
	public double Area() {
		area=latoMaggiore*altezza;
		return area;
	}
	
	public double diagonealeMinore() {
		dMinore= Math.sqrt((latoMinore*latoMinore)-latoMinore*2*(Math.sqrt((latoMaggiore*latoMaggiore)-(altezza*altezza))));
		return dMinore;
		
	}
	
	public Parallelogramma(int latoMaggiore, int latoMinore, int altezza) {
		super();
		this.latoMaggiore = latoMaggiore;
		this.latoMinore = latoMinore;
		this.altezza = altezza;
	}


	public double getLatoMaggiore() {
		return latoMaggiore;
	}

	public void setLatoMaggiore(int latoMaggiore) {
		this.latoMaggiore = latoMaggiore;
	}

	public double getLatoMinore() {
		return latoMinore;
	}

	public void setLatoMinore(int latoMinore) {
		this.latoMinore = latoMinore;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(int altezza) {
		this.altezza = altezza;
	}
	
	
}